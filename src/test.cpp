#include "utility.cpp"
#include <gtest/gtest.h>

TEST(UtilLibrary, Positive)
{
  ASSERT_EQ(0, Something(1));
  ASSERT_EQ(1, Something(2));
  ASSERT_EQ(2, Something(3));
  ASSERT_EQ(3, Something(4));
};

TEST(UtilLibrary, Negative)
{
  ASSERT_EQ(0, Something(-1));
  ASSERT_EQ(0, Something(-2));
  ASSERT_EQ(0, Something(-3));
  ASSERT_EQ(0, Something(-4));
};

TEST(UtilLibrary, Zero)
{
  ASSERT_EQ(0, Something(0));
};

int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
